    console.log('This bot currently requires your discord to have a role called "Jammer" or "DJ" for those who are allowed permission to use the bot.');
    console.log('Loading...');
    //Importing dependencies
    const Discord = require('discord.js');
    const {
        prefix,
        token,
        version
        } = require('./config.json');
    const {
        battleplaylist,
        calmplaylist
        } = require('./playlists.json');
    const ytdl = require('ytdl-core');
    const queue = new Map();

    
    //Creating client and logging in using token
    const client = new Discord.Client();
    client.login(token);

    //Basic listeners
    client.once('ready', () => {
        console.log('Ready. Running music bot version '+version);
        console.log('Closing this program will shut down the bot.');
    });
    client.once('reconnecting', () => {
        console.log('Reconnecting.');
    });
    client.once('disconnect', () => {
        console.log('Disconnect.');
    });

    function shuffle(array) {
        let currentIndex = array.length,  randomIndex;
    
        // While there remain elements to shuffle...
        while (currentIndex != 0) {
    
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
    
        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
        }
    
        return array;
    }

    //Reading Messages
    client.on('message', async message => {
        //Stop if the message was sent by a bot or didn't have the prefix
        if(message.author.bot) return;
        if(!message.content.startsWith(prefix)) return;
        //Stop if the user sending the message doesn't have a DJ or Jammer named role.
        if(!message.member.roles.cache.some(role => role.name == 'DJ') && !message.member.roles.cache.some(role => role.name == 'Jammer')) return message.channel.send("Insufficient Permissions, contact server owner.");;
        
        const serverQueue = queue.get(message.guild.id);

        //check what the user has sent and see if it's a command, return error if it isn't    
        if (message.content.startsWith(`${prefix}play`)) {
            execute(message, serverQueue);
            return;
        } else if (message.content.startsWith(`${prefix}skip`)) {
            skip(message, serverQueue);
            return;
        } else if (message.content.startsWith(`${prefix}stop`)) {
            stop(message, serverQueue);
            return;
        } else if (message.content.startsWith(`${prefix}loopall`)) {
            loopAll(message, serverQueue);
            return;
        } else if (message.content.startsWith(`${prefix}loop`)) {
            loop(message, serverQueue);
            return;
        } else if (message.content.startsWith(`${prefix}battle`)) {
            playlist(message, serverQueue, battleplaylist);
            return;
        } else if (message.content.startsWith(`${prefix}calm`)) {
            playlist(message, serverQueue, calmplaylist);
            return;
        } else {
            message.channel.send("Command not recognized.");
        }
    })

    async function execute(message, serverQueue) {
        //Get content from command.
        const args = message.content.split(" ");

        //Check if an argument was written
        if(args[1] == null) return message.channel.send("Enter song to play.");

        //Check if user is in a voice channel
        const voiceChannel = message.member.voice.channel;
        if(!voiceChannel) return message.channel.send("User not in VC.");

        //Check if bot has speaking and connecting perms
        const permissions = voiceChannel.permissionsFor(message.client.user);
        if(!permissions.has("CONNECT") || !permissions.has("SPEAK")){
            return message.channel.send("Connecting and Speaking permissions required.")
        };

        //Get the song info using the ytdl library
        const songInfo = await ytdl.getInfo(args[1]);
        const song = {
            title: songInfo.videoDetails.title,
            url: songInfo.videoDetails.video_url,
            formats: ytdl.filterFormats(songInfo.formats, 'audioonly'),
        };
        
        addSong(message, serverQueue, voiceChannel, song, songInfo);
    }

    async function playlist(message, serverQueue, playlist){
        //Check if user is in a voice channel
        const voiceChannel = message.member.voice.channel;
        if(!voiceChannel) return message.channel.send("User not in VC.");

        //Check if bot has speaking and connecting perms
        const permissions = voiceChannel.permissionsFor(message.client.user);
        if(!permissions.has("CONNECT") || !permissions.has("SPEAK")){
            return message.channel.send("Connecting and Speaking permissions required.")
        };
        var usedlist = playlist;

        //Feedback to let user know command worked.
        message.channel.send("Initializing playlist.");

        //Shuffle chosen playlist
        shuffle(usedlist);

        //Empty queue if there's one active
        
        if(serverQueue){
            serverQueue.connection.dispatcher.end();
            serverQueue.songs = [];
        } 

        for (const val of usedlist){
            const songInfo = await ytdl.getInfo(val);
            const song = {
                title: songInfo.videoDetails.title,
                url: songInfo.videoDetails.video_url,
                formats: ytdl.filterFormats(songInfo.formats, 'audioonly'),
            };

            await addSong(message, serverQueue, voiceChannel, song, songInfo)
            serverQueue = queue.get(message.guild.id);
        }
        message.channel.send("Playlist Initialized.");
        serverQueue.loopAll = true;
    }

    async function addSong(message, serverQueue, voiceChannel, song, songInfo){
        if(!serverQueue){
            //If queue doesn't exist, create one and add the song.
            const queueConstruct = {
                textChannel: message.channel,
                voiceChannel: voiceChannel,
                connection: null,
                songs: [],
                volume: 5,
                playing: true,
                loop: false,
                loopAll: false,
            };

            // Setting up the queue and pushing song into it.
            queue.set(message.guild.id, queueConstruct);
            queueConstruct.songs.push(song);

            try {
                //Join vc and save connection into an object
                var connection = await voiceChannel.join();
                queueConstruct.connection = connection;

                //Calling play function to start the song
                play(message.guild, queueConstruct.songs[0]);

            } catch (err) {
                console.log(err);
                queue.delete(message.guild.id);
                return message.channel.send(err);
            }
        } else {
            //If queue already exists, add song to it.
            serverQueue.songs.push(song);
            return message.channel.send(`${song.title} has been added to the queue.`);
        };
    }

    //Skip song
    function skip(message, serverQueue){
        if (!message.member.voice.channel)
            return message.channel.send( "You have to be in a voice channel to use this command.");

        if (!serverQueue) 
            return message.channel.send("No song currently playing.");
        serverQueue.connection.dispatcher.end();
        return message.channel.send("Song skipped.");
    }

    //Stop song and kill queue
    function stop(message, serverQueue) {
        if (!message.member.voice.channel)
            return message.channel.send("You have to be in a voice channel to use this command.");
                
        if (!serverQueue)
            return message.channel.send("No song currently playing.");
        
        serverQueue.songs = [];
        serverQueue.connection.dispatcher.end();
        return message.channel.send("Queue killed.")
    }

    //Loop current song
    function loop(message, serverQueue) {
        if (!message.member.voice.channel)
            return message.channel.send("You have to be in a voice channel to use this command.");

        if (!serverQueue)
            return message.channel.send("No song currently playing.");

        //Ensure that you can't loop the queue and the current song
        serverQueue.loopAll = false;

        //Check if currently looping and set it accordingly.
        if(serverQueue.loop){
            serverQueue.loop = false;
            return message.channel.send("Stopping Loop.");
        } else {
            serverQueue.loop = true;
            return message.channel.send("Looping current song.");
        }
            
    }

    //Loop entire Queue
    function loopAll(message, serverQueue){
        if (!message.member.voice.channel)
            return message.channel.send("You have to be in a voice channel to use this command.");

        if (!serverQueue)
            return message.channel.send("No song currently playing.");

        //Ensure that you can't loop the queue and the current song
        serverQueue.loop = false;

        //Check if currently looping and set it accordingly.
        if(serverQueue.loopAll){
            serverQueue.loopAll = false;
            return message.channel.send("Stopping Loop.");
        } else {
            serverQueue.loopAll = true;
            return message.channel.send("Looping queue.");
        }
    }



    function play(guild, song) {
        const serverQueue = queue.get(guild.id);
        //If there's no more songs, delete queue and leave vc
        if(!song) {
            queue.delete(guild.id);
            return;  
        }

        //play song
        const dispatcher = serverQueue.connection
            .play(ytdl(song.url, { filter: 'audioonly'}))
            .on("finish", () => {
                if(!serverQueue.loop) 
                    var lastSong = serverQueue.songs.shift();
                
                if(serverQueue.loopAll)
                    serverQueue.songs.push(lastSong);

                play(guild, serverQueue.songs[0]);
            })
            .on("error", error => console.error(error));
                dispatcher.setVolumeLogarithmic(serverQueue.volume / 5);
                serverQueue.textChannel.send(`Start playing: **${song.title}**`);
    }